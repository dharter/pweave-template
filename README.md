# Pweave Template

An example template of using pweave and pandoc to create book/article like documents suitable for
starting a thesis.  Supports writing using (pandoc) markdown and Pweave to weave literate coding
examples into documents.  Workflow first transforms the pweave markdown (.pmd) into a standard
markdown file (.md).  Then we use pandoc, with its support for latex output and latex header
material, to generate a latex .tex file.  This is then suitable for compilation with latex/pdflatex,
and can include a bibtex bibliography for references.


# Using Templates

To use the templates you need a LaTeX system and BibLaTeX bibliography generation software/libraries.
A makefile is included that will build the example templates if you have LaTeX and
also the rubber LaTeX compilation tool available/installed.

## Linux (Ubuntu) usage

The templates and Makefile were developed on Ubuntu 19.X and 20.X versions of the distributions.  In
general, if you install the latex and latex-live packages, and have the standard build tools
installed, you should be able to build the templates.  You also need Python 3.x installed with the
pweave python library that adds the pweave executable.  I use the anaconda distribution to get
pweave.


1. Install texlive and rubber, also make sure you have basic build tools to build from Makefiles

    $ sudo apt install texlive-base texlive-latex-base texlive-extra-recommended texlive-latex-extra rubber build-essential

2. Install anaconda distribution with most recent Python 3.x version.  pweave is not a standard anaconda package, so perform a 

    $ conda install -c conda-forge pweave 

3. Clone the repository

	$ git clone https://dharter@bitbucket.org/dharter/pweave-template.git

4. Change to the repository directory and do a make to build the example templates.

## Windows 10 usage

I have had success using the following tools.

1. If you want to use the developed Makefile to build the document, need latex and biber tools,
   and of course standard developer tools like make and unix commands.  I have used Cygwin successfully
   for all of these requirements successfully.  Install the Cygwin base and developer categories:
   https://www.cygwin.com/
   
2. To use pweave and pandoc I install Anaconda python packages and do a install of pweave/pandoc

    Conda distribution: https://www.anaconda.com/products/individual
    $ conda install -c conda-forge pweave
    $ conda install -c anaconda pandoc 
   
3. You can probably also build by hand if just using .tex latex files using either 
   [MiKTex](https://miktex.org/download) or [TeXSstudio](https://www.texstudio.org/) for windows.
