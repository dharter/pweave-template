PANDOC_FLAGS=
out := _out
cache := $(out)/.cache
build := $(out)/pdf
mkdir = @mkdir -p $(dir $@)

#TEXINPUTS=".:$(cache):" pdflatex -output-directory="$(cache)" -interaction=nonstopmode -file-line-error $< | grep ":[^:]*:" | grep -v "natbib Warning: Citation"
define pdflatex
TEXINPUTS=".:$(cache):" pdflatex -output-directory="$(cache)" -interaction=nonstopmode -file-line-error $< | grep "Output"
endef

define biber
biber  $(basename $< .tex) | grep "Output"
endef

define rubber
rubber -m pdftex --inplace
endef

define make-depend
./scripts/deps.py $@ < $< > $(cache)/$*.d
endef


## List of all valid targets in this project:
## ------------------------------------------
## all        : by default generate all pdf documents in this repository.
.PHONY : all
all : $(build)/pweave-template.pdf $(build)/thesis.pdf


## thesis.pdf : Generate the thesis.pdf example from
##              markdown/tex templates.
thesis-chapters = chapters/chapter1.tex \
                  chapters/chapter2.tex \
                  chapters/chapter3.tex \
                  chapters/chapter4.tex \
                  chapters/chapter5.tex \
                  chapters/chapter6.tex \
                  chapters/chapter7.tex
thesis-appendices = appendices/appendixA.tex
thesis-figures = figures/example-fig1.png
thesis-bibs = thesis.bib deeplearning.bib
thesis-styles = thesis.cls
thesis-src := thesis.tex $(thesis-chapters) $(thesis-appendices) $(thesis-figures) $(thesis-bibs) $(thesis-styles)
thesis-dest := $(addprefix $(cache)/, $(thesis-src))
$(build)/thesis.pdf : $(thesis-dest)


## pweave-template.pdf : Generate the pweave-template.pdf example
##              from markdown/tex templates.
pweave-template-docs := pweave-template.tex
pweave-template-bibs := pweave-template.bib
pweave-template-figures := figures/example-fig1.png
pweave-template-src := $(pweave-template-docs) $(pweave-template-bibs) $(pweave-template-figures)
pweave-template-dest := $(addprefix $(cache)/, $(pweave-template-src))
$(cache)/pweave-template.tex : PANDOC_FLAGS+=-s -N
$(build)/pweave-template.pdf : $(pweave-template-dest)


$(cache)/%.tex : %.pmd
	@echo "\n\n"
	@echo "************ Weaving Markdown: $< **************"
	$(mkdir)
	pweave -f pandoc -o $(cache)/$*.md $<
	pandoc $(PANDOC_FLAGS) -t latex -o $@ $(cache)/$*.md
	$(make-depend)

$(cache)/%.bib : %.bib
	$(mkdir)
	cp $< $@

$(cache)/%.tex : %.tex
	$(mkdir)
	cp $< $@
	$(make-depend)

$(cache)/%.png : %.png
	$(mkdir)
	cp $< $@

$(cache)/%.sty : %.sty
	$(mkdir)
	cp $< $@

$(cache)/%.cls : %.cls
	$(mkdir)
	cp $< $@

$(build)/%.pdf : $(cache)/%.tex
	@echo "\n\n"
	@echo "************ Building PDF: $@ **************"
	$(mkdir)
	#$(rubber) $<
	$(pdflatex)
	$(biber)
	$(pdflatex)
	$(pdflatex)
	cp $(cache)/$*.pdf $@


## clean      : Remove auto-generated files for a completely clean rebuild.
.PHONY : clean
clean  :
	rm -f *.aux *.log *.out *.bbl *.blg *.lof *.lot *.toc *.equ *~
	rm -rf $(out)
	rm -f thesis.pdf pweave-template.pdf


## help       : Get all build targets supported by this build.
.PHONY : help
help : Makefile
	@sed -n 's/^##//p' $<
